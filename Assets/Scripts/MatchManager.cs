using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;
using Photon.Realtime;
using ExitGames.Client.Photon;
using Random = UnityEngine.Random;

public class MatchManager : MonoBehaviourPunCallbacks, IOnEventCallback
{
    public static MatchManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    public enum EventCodes : byte
    {
        NewPlayer,
        ListPlayers,
        UpdateStat,
        NextMatch,
        TimerSync
    }

    public List<PlayerInfo> allPlayers = new List<PlayerInfo>();
    private int _index;

    private readonly List<LeaderboardPlayer> _leaderboardPlayers = new List<LeaderboardPlayer>();

    public enum GameState
    {
        Waiting,
        Playing,
        Ending
    }

    public int killsToWin = 3;
    public Transform mapCamPoint;
    public GameState state = GameState.Waiting;
    public float waitAfterEnding = 5f;

    public bool perpetual;

    public float matchLength = 180f;
    private float _currentMatchTime;
    private float _sendTimer;

    private void Start()
    {
        if (!PhotonNetwork.IsConnected)
        {
            SceneManager.LoadScene(0);
        }
        else
        {
            NewPlayerSend(PhotonNetwork.NickName);

            state = GameState.Playing;

            SetupTimer();

            if (!PhotonNetwork.IsMasterClient)
            {
                UIController.Instance.timerText.gameObject.SetActive(false);
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab) && state != GameState.Ending)
        {
            if (UIController.Instance.leaderboard.activeInHierarchy)
            {
                UIController.Instance.leaderboard.SetActive(false);
            }
            else
            {
                ShowLeaderboard();
            }
        }

        if (PhotonNetwork.IsMasterClient)
        {
            if (_currentMatchTime > 0f && state == GameState.Playing)
            {
                _currentMatchTime -= Time.deltaTime;

                if (_currentMatchTime <= 0f)
                {
                    _currentMatchTime = 0f;

                    state = GameState.Ending;

                    ListPlayersSend();

                    StateCheck();
                }

                UpdateTimerDisplay();

                _sendTimer -= Time.deltaTime;
                if (_sendTimer <= 0)
                {
                    _sendTimer += 1f;

                    TimerSend();
                }
            }
        }
    }

    public void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code < 200)
        {
            var theEvent = (EventCodes) photonEvent.Code;
            var data = (object[]) photonEvent.CustomData;

            //Debug.Log("Received event " + theEvent);

            switch (theEvent)
            {
                case EventCodes.NewPlayer:

                    NewPlayerReceive(data);

                    break;

                case EventCodes.ListPlayers:

                    ListPlayersReceive(data);

                    break;

                case EventCodes.UpdateStat:

                    UpdateStatsReceive(data);

                    break;

                case EventCodes.NextMatch:

                    NextMatchReceive();

                    break;

                case EventCodes.TimerSync:

                    TimerReceive(data);

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    public override void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    public override void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public void NewPlayerSend(string username)
    {
        var package = new object[4];
        package[0] = username;
        package[1] = PhotonNetwork.LocalPlayer.ActorNumber;
        package[2] = 0;
        package[3] = 0;


        PhotonNetwork.RaiseEvent(
            (byte) EventCodes.NewPlayer,
            package,
            new RaiseEventOptions {Receivers = ReceiverGroup.MasterClient},
            new SendOptions {Reliability = true}
        );
    }

    public void NewPlayerReceive(object[] dataReceived)
    {
        var player = new PlayerInfo((string) dataReceived[0], (int) dataReceived[1], (int) dataReceived[2],
            (int) dataReceived[3]);

        allPlayers.Add(player);

        ListPlayersSend();
    }

    public void ListPlayersSend()
    {
        var package = new object[allPlayers.Count + 1];

        package[0] = state;

        for (var i = 0; i < allPlayers.Count; i++)
        {
            var piece = new object[4];

            piece[0] = allPlayers[i].name;
            piece[1] = allPlayers[i].actor;
            piece[2] = allPlayers[i].kills;
            piece[3] = allPlayers[i].deaths;

            package[i + 1] = piece;
        }

        PhotonNetwork.RaiseEvent(
            (byte) EventCodes.ListPlayers,
            package,
            new RaiseEventOptions {Receivers = ReceiverGroup.All},
            new SendOptions {Reliability = true}
        );
    }

    public void ListPlayersReceive(object[] dataReceived)
    {
        allPlayers.Clear();

        state = (GameState) dataReceived[0];

        for (var i = 1; i < dataReceived.Length; i++)
        {
            var piece = (object[]) dataReceived[i];

            var player = new PlayerInfo(
                (string) piece[0],
                (int) piece[1],
                (int) piece[2],
                (int) piece[3]
            );

            allPlayers.Add(player);

            if (PhotonNetwork.LocalPlayer.ActorNumber == player.actor)
            {
                _index = i - 1;
            }
        }

        StateCheck();
    }

    public void UpdateStatsSend(int actorSending, int statToUpdate, int amountToChange)
    {
        var package = new object[] {actorSending, statToUpdate, amountToChange};

        PhotonNetwork.RaiseEvent(
            (byte) EventCodes.UpdateStat,
            package,
            new RaiseEventOptions {Receivers = ReceiverGroup.All},
            new SendOptions {Reliability = true}
        );
    }

    public void UpdateStatsReceive(object[] dataReceived)
    {
        var actor = (int) dataReceived[0];
        var statType = (int) dataReceived[1];
        var amount = (int) dataReceived[2];

        for (var i = 0; i < allPlayers.Count; i++)
        {
            if (allPlayers[i].actor == actor)
            {
                switch (statType)
                {
                    case 0: //kills
                        allPlayers[i].kills += amount;
                        Debug.Log("Player " + allPlayers[i].name + " : kills " + allPlayers[i].kills);
                        break;

                    case 1: //deaths
                        allPlayers[i].deaths += amount;
                        Debug.Log("Player " + allPlayers[i].name + " : deaths " + allPlayers[i].deaths);
                        break;
                }

                if (i == _index)
                {
                    UpdateStatsDisplay();
                }

                if (UIController.Instance.leaderboard.activeInHierarchy)
                {
                    ShowLeaderboard();
                }

                break;
            }
        }

        ScoreCheck();
    }

    public void UpdateStatsDisplay()
    {
        if (allPlayers.Count > _index)
        {
            UIController.Instance.killsText.text = "Kills: " + allPlayers[_index].kills;
            UIController.Instance.deathsText.text = "Deaths: " + allPlayers[_index].deaths;
        }
        else
        {
            UIController.Instance.killsText.text = "Kills: 0";
            UIController.Instance.deathsText.text = "Deaths: 0";
        }
    }

    private void ShowLeaderboard()
    {
        UIController.Instance.leaderboard.SetActive(true);

        foreach (LeaderboardPlayer lp in _leaderboardPlayers)
        {
            Destroy(lp.gameObject);
        }

        _leaderboardPlayers.Clear();

        UIController.Instance.leaderboardPlayerDisplay.gameObject.SetActive(false);

        var sorted = SortPlayers(allPlayers);

        foreach (PlayerInfo player in sorted)
        {
            LeaderboardPlayer newPlayerDisplay = Instantiate(UIController.Instance.leaderboardPlayerDisplay,
                UIController.Instance.leaderboardPlayerDisplay.transform.parent);

            newPlayerDisplay.SetDetails(player.name, player.kills, player.deaths);

            newPlayerDisplay.gameObject.SetActive(true);

            _leaderboardPlayers.Add(newPlayerDisplay);
        }
    }

    private List<PlayerInfo> SortPlayers(List<PlayerInfo> players)
    {
        var sorted = new List<PlayerInfo>();

        while (sorted.Count < players.Count)
        {
            int highest = -1;
            PlayerInfo selectedPlayer = players[0];

            foreach (PlayerInfo player in players.Where(player => !sorted.Contains(player)).Where(player => player.kills > highest))
            {
                selectedPlayer = player;
                highest = player.kills;
            }

            sorted.Add(selectedPlayer);
        }

        return sorted;
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();

        SceneManager.LoadScene(0);
    }

    private void ScoreCheck()
    {
        bool winnerFound = allPlayers.Any(player => player.kills >= killsToWin && killsToWin > 0);

        if (!winnerFound)
            return;
        
        if (!PhotonNetwork.IsMasterClient || state == GameState.Ending) 
            return;
        
        state = GameState.Ending;
        ListPlayersSend();
    }

    private void StateCheck()
    {
        if (state == GameState.Ending)
        {
            EndGame();
        }
    }

    private void EndGame()
    {
        state = GameState.Ending;

        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.DestroyAll();
        }

        UIController.Instance.endScreen.SetActive(true);
        ShowLeaderboard();

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        if (Camera.main is { })
        {
            Transform transform1 = Camera.main.transform;
            transform1.position = mapCamPoint.position;
            transform1.rotation = mapCamPoint.rotation;
        }

        StartCoroutine(EndCo());
    }

    private IEnumerator EndCo()
    {
        yield return new WaitForSeconds(waitAfterEnding);

        if (!perpetual)
        {
            PhotonNetwork.AutomaticallySyncScene = false;
            PhotonNetwork.LeaveRoom();
        }
        else
        {
            if (!PhotonNetwork.IsMasterClient)
                yield break;
            if (!Launcher.Instance.changeMapBetweenRounds)
            {
                NextMatchSend();
            }
            else
            {
                int newLevel = Random.Range(0, Launcher.Instance.allMaps.Length);

                if (Launcher.Instance.allMaps[newLevel] == SceneManager.GetActiveScene().name)
                {
                    NextMatchSend();
                }
                else
                {
                    PhotonNetwork.LoadLevel(Launcher.Instance.allMaps[newLevel]);
                }
            }
        }
    }

    public void NextMatchSend()
    {
        PhotonNetwork.RaiseEvent(
            (byte) EventCodes.NextMatch,
            null,
            new RaiseEventOptions {Receivers = ReceiverGroup.All},
            new SendOptions {Reliability = true}
        );
    }

    public void NextMatchReceive()
    {
        state = GameState.Playing;

        UIController.Instance.endScreen.SetActive(false);
        UIController.Instance.leaderboard.SetActive(false);

        foreach (PlayerInfo player in allPlayers)
        {
            player.kills = 0;
            player.deaths = 0;
        }

        UpdateStatsDisplay();

        PlayerSpawner.Instance.SpawnPlayer();

        SetupTimer();
    }

    public void SetupTimer()
    {
        if (!(matchLength > 0))
            return;
        
        _currentMatchTime = matchLength;
        UpdateTimerDisplay();
    }

    public void UpdateTimerDisplay()
    {
        TimeSpan timeToDisplay = System.TimeSpan.FromSeconds(_currentMatchTime);

        UIController.Instance.timerText.text =
            timeToDisplay.Minutes.ToString("00") + ":" + timeToDisplay.Seconds.ToString("00");
    }

    public void TimerSend()
    {
        var package = new object[] {(int) _currentMatchTime, state};

        PhotonNetwork.RaiseEvent(
            (byte) EventCodes.TimerSync,
            package,
            new RaiseEventOptions {Receivers = ReceiverGroup.All},
            new SendOptions {Reliability = true}
        );
    }

    public void TimerReceive(object[] dataReceived)
    {
        _currentMatchTime = (int) dataReceived[0];
        state = (GameState) dataReceived[1];

        UpdateTimerDisplay();

        UIController.Instance.timerText.gameObject.SetActive(true);
    }
}

[System.Serializable]
public class PlayerInfo
{
    public string name;
    public int actor, kills, deaths;

    public PlayerInfo(string name, int actor, int kills, int deaths)
    {
        this.name = name;
        this.actor = actor;
        this.kills = kills;
        this.deaths = deaths;
    }
}