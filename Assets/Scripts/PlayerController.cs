using UnityEngine;
using Photon.Pun;

public class PlayerController : MonoBehaviourPunCallbacks
{
    public Transform viewPoint;

    [SerializeField] private float mouseSensitivity = 1.0f;
    [SerializeField] private float jumpForce = 12.0f;
    [SerializeField] private float gravityMod = 2.5f;


    [SerializeField] private bool invertLook;
    [SerializeField] private float moveSpeed = 5.0f;

    [SerializeField] private float runSpeed = 8.0f;


    public CharacterController charCon;


    public Transform groundCheckPoint;

    public LayerMask groundLayers;
    public GameObject bulletImpact;

    [SerializeField] private float muzzleDisplayTime = 0.0166666666f;

    [SerializeField] private float maxHeat = 10f;
    [SerializeField] private float coolRate = 4f;
    [SerializeField] private float overheatCoolRate = 5f;


    public Gun[] allGuns;


    public GameObject playerHitImpact;
    [SerializeField] private int maxHealth = 100;

    public Animator anim;
    public GameObject playerModel;
    public Transform modelGunPoint, gunHolder;
    public Material[] allSkins;

    [SerializeField] private float adsSpeed = 5f;

    public Transform adsOutPoint;
    public Transform adsInPoint;
    public AudioSource footstepSlow;
    public AudioSource footstepFast;
    private static readonly int Grounded = Animator.StringToHash("grounded");
    private static readonly int Speed = Animator.StringToHash("speed");

    private int _currentHealth;
    private int _selectedGun;
    private float _heatCounter;
    private bool _overHeated;
    private float _muzzleCounter;
    private Camera _cam;
    private bool _isGrounded;
    private float _verticalRotStore;
    private Vector2 _mouseInput;
    private float _activeMoveSpeed;
    private Vector3 _moveDir;
    private Vector3 _movement;
    private float _shotCounter;


    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        _cam = Camera.main;

        UIController.Instance.weaponTempSlider.maxValue = maxHeat;

        photonView.RPC(nameof(SetGun), RpcTarget.All, _selectedGun);

        _currentHealth = maxHealth;

        if (photonView.IsMine)
        {
            playerModel.SetActive(false);

            UIController.Instance.healthSlider.maxValue = maxHealth;
            UIController.Instance.healthSlider.value = _currentHealth;
        }
        else
        {
            gunHolder.parent = modelGunPoint;
            gunHolder.localPosition = Vector3.zero;
            gunHolder.localRotation = Quaternion.identity;
        }

        playerModel.GetComponent<Renderer>().material = allSkins[photonView.Owner.ActorNumber % allSkins.Length];
    }

    private void Update()
    {
        if (!photonView.IsMine)
            return;

        ShouldBeRefactor();
    }

    // ReSharper disable Unity.PerformanceAnalysis
    private void ShouldBeRefactor()
    {
        _mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y")) * mouseSensitivity;

        Quaternion rotation = transform.rotation;
        rotation = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y + _mouseInput.x,
            rotation.eulerAngles.z);
        transform.rotation = rotation;

        _verticalRotStore += _mouseInput.y;
        _verticalRotStore = Mathf.Clamp(_verticalRotStore, -60f, 60f);

        Look();

        _moveDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));

        if (Input.GetKey(KeyCode.LeftShift))
        {
            _activeMoveSpeed = runSpeed;

            if (!footstepFast.isPlaying && _moveDir != Vector3.zero)
            {
                footstepFast.Play();
                footstepSlow.Stop();
            }
        }
        else
        {
            _activeMoveSpeed = moveSpeed;

            if (!footstepSlow.isPlaying && _moveDir != Vector3.zero)
            {
                footstepFast.Stop();
                footstepSlow.Play();
            }
        }

        if (_moveDir == Vector3.zero || !_isGrounded)
        {
            footstepSlow.Stop();
            footstepFast.Stop();
        }

        float yVel = _movement.y;
        Transform transform1 = transform;
        _movement = (transform1.forward * _moveDir.z + transform1.right * _moveDir.x).normalized * _activeMoveSpeed;
        _movement.y = yVel;

        if (charCon.isGrounded)
        {
            _movement.y = 0f;
        }

        _isGrounded = Physics.Raycast(groundCheckPoint.position, Vector3.down, .25f, groundLayers);

        if (Input.GetButtonDown("Jump") && _isGrounded)
        {
            _movement.y = jumpForce;
        }

        _movement.y += Physics.gravity.y * Time.deltaTime * gravityMod;

        charCon.Move(_movement * Time.deltaTime);

        if (allGuns[_selectedGun].muzzleFlash.activeInHierarchy)
        {
            _muzzleCounter -= Time.deltaTime;

            if (_muzzleCounter <= 0)
            {
                allGuns[_selectedGun].muzzleFlash.SetActive(false);
            }
        }

        ShootWithOverHeat();
        UIController.Instance.weaponTempSlider.value = _heatCounter;
        float axisRaw = Input.GetAxisRaw("Mouse ScrollWheel");
        SwitchGunAfterScroll(axisRaw);
        SwitchGunAfterPressHotKey();
        anim.SetBool(Grounded, _isGrounded);
        anim.SetFloat(Speed, _moveDir.magnitude);
        ZoomScopeOfGun();
        SwitchStateOfCursorWhenPressEscape();
    }

    private static void SwitchStateOfCursorWhenPressEscape()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        else if (Cursor.lockState == CursorLockMode.None)
        {
            if (Input.GetMouseButtonDown(0) && !UIController.Instance.optionsScreen.activeInHierarchy)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }

    private void Look()
    {
        Quaternion rotation = viewPoint.rotation;
        float value = _verticalRotStore * (invertLook ? 1.0f : -1.0f);
        rotation = Quaternion.Euler(value, rotation.eulerAngles.y, rotation.eulerAngles.z);
        viewPoint.rotation = rotation;
    }

    private void ShootWithOverHeat()
    {
        if (_overHeated)
        {
            _heatCounter -= overheatCoolRate * Time.deltaTime;
            if (_heatCounter <= 0)
            {
                _overHeated = false;

                UIController.Instance.overheatedMessage.gameObject.SetActive(false);
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                Shoot();
            }

            if (Input.GetMouseButton(0) && allGuns[_selectedGun].isAutomatic)
            {
                _shotCounter -= Time.deltaTime;

                if (_shotCounter <= 0)
                {
                    Shoot();
                }
            }

            _heatCounter -= coolRate * Time.deltaTime;
        }

        if (_heatCounter < 0)
        {
            _heatCounter = 0f;
        }
    }

    private void SwitchGunAfterPressHotKey()
    {
        for (var i = 0; i < allGuns.Length; i++)
        {
            if (!Input.GetKeyDown((i + 1).ToString()))
                continue;

            _selectedGun = i;
            photonView.RPC(nameof(SetGun), RpcTarget.All, _selectedGun);
        }
    }

    private void ZoomScopeOfGun()
    {
        if (Input.GetMouseButton(1))
        {
            _cam.fieldOfView = Mathf.Lerp(_cam.fieldOfView, allGuns[_selectedGun].adsZoom, adsSpeed * Time.deltaTime);
            gunHolder.position = Vector3.Lerp(gunHolder.position, adsInPoint.position, adsSpeed * Time.deltaTime);
        }
        else
        {
            _cam.fieldOfView = Mathf.Lerp(_cam.fieldOfView, 60f, adsSpeed * Time.deltaTime);
            gunHolder.position = Vector3.Lerp(gunHolder.position, adsOutPoint.position, adsSpeed * Time.deltaTime);
        }
    }


    private void Shoot()
    {
        Ray ray = _cam.ViewportPointToRay(new Vector3(.5f, .5f, 0f));
        ray.origin = _cam.transform.position;

        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                Debug.Log("Hit " + hit.collider.gameObject.GetPhotonView().Owner.NickName);

                PhotonNetwork.Instantiate(playerHitImpact.name, hit.point, Quaternion.identity);

                hit.collider.gameObject.GetPhotonView().RPC(nameof(DealDamage), RpcTarget.All,
                    photonView.Owner.NickName,
                    allGuns[_selectedGun].shotDamage, PhotonNetwork.LocalPlayer.ActorNumber);
            }
            else
            {
                GameObject bulletImpactObject = Instantiate(bulletImpact, hit.point + hit.normal * .002f,
                    Quaternion.LookRotation(hit.normal, Vector3.up));

                Destroy(bulletImpactObject, 10f);
            }
        }

        _shotCounter = allGuns[_selectedGun].timeBetweenShots;


        _heatCounter += allGuns[_selectedGun].heatPerShot;
        if (_heatCounter >= maxHeat)
        {
            _heatCounter = maxHeat;

            _overHeated = true;

            UIController.Instance.overheatedMessage.gameObject.SetActive(true);
        }

        allGuns[_selectedGun].muzzleFlash.SetActive(true);
        _muzzleCounter = muzzleDisplayTime;

        allGuns[_selectedGun].shotSound.Stop();
        allGuns[_selectedGun].shotSound.Play();
    }

    [PunRPC]
    public void DealDamage(string damager, int damageAmount, int actor)
    {
        TakeDamage(damager, damageAmount, actor);
    }

    private void TakeDamage(string damager, int damageAmount, int actor)
    {
        if (!photonView.IsMine)
            return;

        _currentHealth -= damageAmount;

        if (_currentHealth <= 0)
        {
            _currentHealth = 0;

            PlayerSpawner.Instance.Die(damager);

            MatchManager.Instance.UpdateStatsSend(actor, 0, 1);
        }


        UIController.Instance.healthSlider.value = _currentHealth;
    }

    private void LateUpdate()
    {
        if (!photonView.IsMine)
            return;
        
        Transform transform1 = _cam.transform;
        if (MatchManager.Instance.state == MatchManager.GameState.Playing)
        {
            transform1.position = viewPoint.position;
            transform1.rotation = viewPoint.rotation;
        }
        else
        {
            transform1.position = MatchManager.Instance.mapCamPoint.position;
            transform1.rotation = MatchManager.Instance.mapCamPoint.rotation;
        }
    }

    private void SwitchGunAfterScroll(float axisRaw)
    {
        if (axisRaw > 0f)
        {
            _selectedGun++;

            if (_selectedGun >= allGuns.Length)
            {
                _selectedGun = 0;
            }

            photonView.RPC(nameof(SetGun), RpcTarget.All, _selectedGun);
        }
        else if (axisRaw < 0f)
        {
            _selectedGun--;

            if (_selectedGun < 0)
            {
                _selectedGun = allGuns.Length - 1;
            }

            photonView.RPC(nameof(SetGun), RpcTarget.All, _selectedGun);
        }
    }

    [PunRPC]
    public void SetGun(int gunToSwitchTo)
    {
        if (gunToSwitchTo >= allGuns.Length)
            return;

        _selectedGun = gunToSwitchTo;
        SwitchGun();
    }

    private void SwitchGun()
    {
        foreach (Gun gun in allGuns)
        {
            gun.gameObject.SetActive(false);
        }

        allGuns[_selectedGun].gameObject.SetActive(true);

        allGuns[_selectedGun].muzzleFlash.SetActive(false);
    }
}