using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSpawner : MonoBehaviour
{
    public static PlayerSpawner Instance;

    private void Awake()
    {
        Instance = this;
    }

    public GameObject playerPrefab;
    private GameObject _player;

    public GameObject deathEffect;

    public float respawnTime = 5f;

    // Start is called before the first frame update
    private void Start()
    {
        if (PhotonNetwork.IsConnected)
        {
            SpawnPlayer();
        }
    }

    public void SpawnPlayer()
    {
        Transform spawnPoint = SpawnManager.Instance.GetSpawnPoint();

        _player = PhotonNetwork.Instantiate(playerPrefab.name, spawnPoint.position, spawnPoint.rotation);
    }

    public void Die(string damager)
    {
        UIController.Instance.deathText.text = "You were killed by " + damager;
        
        MatchManager.Instance.UpdateStatsSend(PhotonNetwork.LocalPlayer.ActorNumber, 1, 1);

        if (_player != null)
        {
            StartCoroutine(DieCo());
        }
    }

    public IEnumerator DieCo()
    {
        PhotonNetwork.Instantiate(deathEffect.name, _player.transform.position, Quaternion.identity);

        PhotonNetwork.Destroy(_player);
        _player = null;
        UIController.Instance.deathScreen.SetActive(true);

        yield return new WaitForSeconds(respawnTime);

        UIController.Instance.deathScreen.SetActive(false);

        if (MatchManager.Instance.state == MatchManager.GameState.Playing && _player == null)
        {
            SpawnPlayer();
        }
    }
}